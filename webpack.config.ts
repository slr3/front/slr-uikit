import path from 'node:path'
import { VueLoaderPlugin } from 'vue-loader'
import { fileURLToPath } from 'url';
import MiniCssExtractPlugin from 'mini-css-extract-plugin'
import CssMinimizerPlugin from 'css-minimizer-webpack-plugin'

import {Configuration} from 'webpack';

const __filename = fileURLToPath(import.meta.url);

const __dirname = path.dirname(__filename);

const config: Configuration = {
  entry: './src/index.ts',
  experiments: {
    outputModule: true,
  },
  output: {
    pathinfo: true,
    path: path.resolve(__dirname, 'dist'),
    auxiliaryComment: 'Test Comment',
    chunkFormat: 'module',
    module: true,
    asyncChunks: true,
    clean: true,
    crossOriginLoading: 'anonymous',
    libraryTarget: 'module',
    scriptType: 'module',
    filename: 'uikit.js',
    sourceMapFilename: 'uikit.js.map',
  },
  mode: 'production',
  resolve: {
    extensions: ['.ts', '.js', '.json', '.vue'],
    alias: {
      '@': path.resolve(__dirname, './src/'),
    },
  },
  module: {
    rules: [
      {
        test: /\.vue$/,
        loader: 'vue-loader',
      },
      {
        test: /\.tsx?$/,
        loader: 'ts-loader',
        exclude: /node_modules/,
        options: {
          appendTsSuffixTo: [/\.vue$/]
        }
      },
      {
        test: /\.js$/,
        loader: 'babel-loader',
      },
      {
        test: /\.css$/,
        use: [
          { loader: MiniCssExtractPlugin.loader },
          { loader: "css-loader", options: { sourceMap: true } },
          { loader: "postcss-loader", options: { sourceMap: true } },
        ]
      },
    ]
  },
  plugins: [
    new VueLoaderPlugin(),
    new MiniCssExtractPlugin({
      filename: 'uikit.css',
    }),
  ],
  optimization: {
    minimizer: [
      // For webpack@5 you can use the `...` syntax to extend existing minimizers (i.e. `terser-webpack-plugin`), uncomment the next line
      '...',
      new CssMinimizerPlugin({
        minimizerOptions: {
          preset: [
            "default",
            {
              discardComments: { removeAll: true },
            },
          ],
        },
        minify: CssMinimizerPlugin.cssnanoMinify,
      }),
    ],
  },
  externals: [
    'vue',
    'vue-router',
    '@vueuse/shared',
    '@vueuse/core',
  ]
}

export default config
