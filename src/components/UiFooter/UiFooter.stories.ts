import UiFooter from './UiFooter.vue';

// More on default export: https://storybook.js.org/docs/vue/writing-stories/introduction#default-export
export default {
  title: 'Example/UiFooter',
  component: UiFooter,
}

const Template = (args) => ({
  components: { UiFooter },
  setup() {
    return { args };
  },
  template: `
    <div style="width: 100%">
      <UiFooter v-bind="args"/>
    </div>
  `,
})

export const Primary = Template.bind({})

Primary.args = {
  links: [
    {
      label: 'About',
      items: [
        {
          text: 'first',
          href: '#',
        },
        {
          text: 'second',
          href: '#',
        },
      ],
    },
    {
      label: 'About 2',
      items: [
        {
          text: 'first',
          href: '#',
        },
        {
          text: 'second',
          href: '#',
        },
      ],
    },
    {
      label: 'About 3',
      items: [
        {
          text: 'first',
          href: '#',
        },
        {
          text: 'second',
          href: '#',
        },
      ],
    },
  ],
}
