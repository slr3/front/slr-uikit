import UiLinkExternal from './UiLinkExternal.vue';

// More on default export: https://storybook.js.org/docs/vue/writing-stories/introduction#default-export
export default {
  title: 'Example/UiLinkExternal',
  component: UiLinkExternal,
};

const Template = (args) => ({
  components: { UiLinkExternal },
  setup() {
    return { args };
  },
  template: '<UiLinkExternal v-bind="args" />',
})

export const Primary = Template.bind({})

Primary.args = {
  text: 'Example External link text',
  href: '#',
}
