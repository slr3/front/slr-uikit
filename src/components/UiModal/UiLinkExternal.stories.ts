import { ref } from 'vue'
import UiModal from './UiModal.vue'
import { UiButton } from '../UiButton'

// More on default export: https://storybook.js.org/docs/vue/writing-stories/introduction#default-export
export default {
  title: 'Example/UiModal',
  component: UiModal,
};

const Template = (args) => ({
  components: { UiModal, UiButton },
  setup() {
    const isOpen = ref(false)

    return { args, isOpen };
  },
  template: `
    <div>
      <UiButton @click="isOpen = true">Open</UiButton>
      <UiModal v-bind="args" v-model="isOpen">
        Content
      </Uimodal>
    </div>
  `,
})

export const Primary = Template.bind({})

Primary.args = {
  label: 'Example modal',
  closable: true,
}
