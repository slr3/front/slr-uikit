import UiAccordion from './UiAccordion.vue';
import UiAccordionStyled from './UiAccordionStyled.vue';
import { ref } from 'vue'

// More on default export: https://storybook.js.org/docs/vue/writing-stories/introduction#default-export
export default {
  title: 'Example/UiAccordion',
  component: UiAccordion,
};

const Template = (args) => ({
  components: { UiAccordion },
  setup() {
    const value = ref(0)

    return { args, value }
  },
  template: `
    <div style="width: 300px">
      <ui-accordion title="Where do I get my referral link?">
        Connect a wallet and find your referral link in the Referral section.
      </ui-accordion>
    </div>`,
})

const TemplateStyled = (args) => ({
  components: { UiAccordionStyled },
  setup() {
    const value = ref(0)

    return { args, value }
  },
  template: `
    <div style="width: 300px">
      <ui-accordion-styled title="Where do I get my referral link?">
        Connect a wallet and find your referral link in the Referral section.
      </ui-accordion-styled>
    </div>`,
})

export const Primary = Template.bind({})
export const Styled = TemplateStyled.bind({})
