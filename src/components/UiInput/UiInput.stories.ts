import UiInput from './UiInput.vue';

// More on default export: https://storybook.js.org/docs/vue/writing-stories/introduction#default-export
export default {
  title: 'Example/UiInput',
  component: UiInput,
};

const Template = (args) => ({
  components: { UiInput },
  setup() {
    return { args };
  },
  template: `
    <div style="width: 600px; min-width:200px">
      <UiInput v-bind="args"><template #content>50</template></UiInput>
    </div>
  `,
})

export const Primary = Template.bind({})

Primary.args = {
  placeholder: 'Placeholder',
  inputmode: 'text',
}
