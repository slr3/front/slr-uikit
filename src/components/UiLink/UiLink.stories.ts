import UiLink from './UiLink.vue';

// More on default export: https://storybook.js.org/docs/vue/writing-stories/introduction#default-export
export default {
  title: 'Example/UiLink',
  component: UiLink,
};

const Template = (args) => ({
  components: { UiLink },
  setup() {
    return { args };
  },
  template: '<UiLink v-bind="args" />',
})

export const Primary = Template.bind({})

Primary.args = {
  text: 'Example link text',
  href: '#',
}
