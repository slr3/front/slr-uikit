import { UiClickableHrefProp, UiClickableTargetProp, UiClickableToProp } from '../UiClickable'

export type UiLinkTextProp = string
export type UiLinkToProp = UiClickableToProp
export type UiLinkHrefProp = UiClickableHrefProp
export type UiLinkTargetProp = UiClickableTargetProp

export type UiLinkProps = {
  text?: UiLinkTextProp
  to?: UiLinkToProp
  href?: UiLinkHrefProp
  target?: UiLinkTargetProp
}
