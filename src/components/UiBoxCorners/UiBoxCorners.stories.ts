import UiBoxCorners from './UiBoxCorners.vue';
import { ref } from 'vue'

// More on default export: https://storybook.js.org/docs/vue/writing-stories/introduction#default-export
export default {
  title: 'Example/UiBoxCorners',
  component: UiBoxCorners,
};

const Template = (args) => ({
  components: { UiBoxCorners },
  setup() {
    const value = ref(0)

    return { args, value }
  },
  template: `
    <div style="width: 300px">
      <ui-box-corners style="padding: 24px">
        Connect a wallet and find your referral link in the Referral section.
      </ui-box-corners>
    </div>`,
})

export const Primary = Template.bind({})
