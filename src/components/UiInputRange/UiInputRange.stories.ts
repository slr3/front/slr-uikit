import UiInputRange from './UiInputRange.vue';
import { ref } from 'vue'

// More on default export: https://storybook.js.org/docs/vue/writing-stories/introduction#default-export
export default {
  title: 'Example/UiInputRange',
  component: UiInputRange,
};

const Template = (args) => ({
  components: { UiInputRange },
  setup() {
    const value = ref(0)

    return { args, value }
  },
  template: `
    <div style="width: 300px">
      <UiInputRange v-bind="args" v-model:value="value" />
    </div>`,
})

export const Primary = Template.bind({})

Primary.args = {
  min: -100,
  max: 100,
  step: 1,
}

export const IsLoading = Template.bind({})

IsLoading.args = {
  min: -100,
  max: 100,
  step: 1,
  isLoading: true,
}
