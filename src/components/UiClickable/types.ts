import type { RouteLocationRaw } from 'vue-router'
import type { UI_CLICKABLE_TYPE } from './constants'

export type UiClickableTypeProp = typeof UI_CLICKABLE_TYPE[number]
export type UiClickableTargetProp = HTMLAnchorElement['target']
export type UiClickableToProp = RouteLocationRaw
export type UiClickableHrefProp = string

export type UiClickableProps ={
  to?: RouteLocationRaw
  type?: UiClickableTypeProp
  target?: UiClickableTargetProp,
  href?: UiClickableHrefProp
  disabled?: boolean
}
