import UiPoligonButton from './UiPoligonButton.vue';

// More on default export: https://storybook.js.org/docs/vue/writing-stories/introduction#default-export
export default {
  title: 'Example/UiPoligonButton',
  component: UiPoligonButton,
};

const Template = (args) => ({
  components: { UiPoligonButton },
  setup() {
    const sizes = [64, 48, 36, 32, 24]
    return { args, sizes };
  },
  template: `
    <div class="uikit-flex flex-wrap uikit-space-x-32">
      <div class="uikit-flex uikit-flex-col uikit-items-center uikit-space-y-12">
        <UiPoligonButton
          v-for="size in sizes"
          v-bind="args"
          :size="size"
          :key="size"
        >
          SLR!
        </UiPoligonButton>
      </div>
      <div class="uikit-flex uikit-flex-col uikit-items-center uikit-space-y-12">
        <UiPoligonButton
          v-for="size in sizes"
          v-bind="args"
          :size="size"
          :key="size"
        >
          SLR!
        </UiPoligonButton>
      </div>
    </div>
  `,
})

export const Primary = Template.bind({})

export const White = Template.bind({})
White.args = { variant: 'white' }

export const Violet = Template.bind({})
Violet.args = { variant: 'violet' }

export const GreenOriginal = Template.bind({})
GreenOriginal.args = { variant: 'green-original' }

export const Red = Template.bind({})
Red.args = { variant: 'red' }
