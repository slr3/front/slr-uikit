import type { UiButtonProps } from "../UiButton"

export type UiFooterTokenWidgetProps = {
  price: number,
  totalSupply: number,
  totalBlocked: number,
  circulatingSupply: number,
  totalBurned: number,
  marketCap: number,
  action: UiButtonProps,
}
