import UiFooterTokenWidget from './UiFooterTokenWidget.vue';

// More on default export: https://storybook.js.org/docs/vue/writing-stories/introduction#default-export
export default {
  title: 'Example/UiFooterTokenWidget',
  component: UiFooterTokenWidget,
}

const Template = (args) => ({
  components: { UiFooterTokenWidget },
  setup() {
    return { args };
  },
  template: `
    <div style="width: 100%">
      <UiFooterTokenWidget v-bind="args"/>
    </div>
  `,
})

export const Primary = Template.bind({})
