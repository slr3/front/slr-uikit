import UiProgress from './UiProgress.vue';

// More on default export: https://storybook.js.org/docs/vue/writing-stories/introduction#default-export
export default {
  title: 'Example/UiProgress',
  component: UiProgress,
};

const Template = (args) => ({
  components: { UiProgress },
  setup() {
    return { args };
  },
  template: `
    <div style="width: 600px; min-width:200px">
      <UiProgress v-bind="args"><template #content>50</template></UiProgress>
    </div>
  `,
})

export const Primary = Template.bind({})

Primary.args = {
  value: 50,
  max: 100,
  min: 0,
  text: 'Example hidden text',
}
