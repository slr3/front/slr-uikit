import type { UI_POLIGON_BUTTON_VARIANTS } from './constants'

export type UiPoligonVariantProp = typeof UI_POLIGON_BUTTON_VARIANTS[number]
