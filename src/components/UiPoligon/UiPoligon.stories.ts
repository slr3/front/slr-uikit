import UiPoligon from './UiPoligon.vue';
import { UI_POLIGON_BUTTON_VARIANTS } from './constants'

// More on default export: https://storybook.js.org/docs/vue/writing-stories/introduction#default-export
export default {
  title: 'Example/UiPoligon',
  component: UiPoligon,
};

const Template = (args) => ({
  components: { UiPoligon },
  setup() {
    return { args, variants: UI_POLIGON_BUTTON_VARIANTS };
  },
  template: `
    <div>
      <UiPoligon
        v-bind="args"
        v-for="variant in variants"
        :variant="variant"
        :key="variant"
      >
        SLR
      </UiPoligon>
    </div>`,
})

export const Primary = Template.bind({})

export const Animated = Template.bind({})
Animated.args= { animated: true }
