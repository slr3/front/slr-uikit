import UiTextPlaceholder from './UiTextPlaceholder.vue';

// More on default export: https://storybook.js.org/docs/vue/writing-stories/introduction#default-export
export default {
  title: 'Example/UiTextPlaceholder',
  component: UiTextPlaceholder,
};

const Template = (args) => ({
  components: { UiTextPlaceholder },
  setup() {
    return { args };
  },
  template: '<UiTextPlaceholder v-bind="args" style="color: #fff"/>',
})

export const Primary = Template.bind({})

Primary.args = {
  isLoading: true,
  text: 'Example hidden text',
  textLength: 19,
}
