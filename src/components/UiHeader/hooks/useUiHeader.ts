import { ref, Ref } from 'vue'
import { getDocument } from '../../../utils/getElement'

export enum UiHeaderType {
  DEFAULT_DESKTOP,
  DEFAULT_MOBILE,
}

const HEADER_SIZE = {
  [UiHeaderType.DEFAULT_DESKTOP]: {
    height: 96,
    offset: 64,
  },
  [UiHeaderType.DEFAULT_MOBILE]: {
    height: 72,
    offset: 16,
  },
}

const size = ref({
  height: 0,
  offset: 0,
})

const type = ref() as Ref<UiHeaderType | undefined>

export const useUiHeader = () => {
  const setHeaderType = (headerType: UiHeaderType) => {
    const document = getDocument()

    if (!document) {
      return
    }


    const headerSize = HEADER_SIZE[headerType]
    type.value = headerType
    size.value = headerSize

    if (typeof window !== 'undefined' && window.document) {
      document.documentElement.style.setProperty('--app-ui-header-height', `${headerSize.height}px`)
      document.documentElement.style.setProperty('--app-ui-header-offset', `${headerSize.offset}px`)
    }

    if (headerType === UiHeaderType.DEFAULT_DESKTOP) {
      document.documentElement.style.setProperty('--app-ui-header-base-height', '64px')
      document.documentElement.style.setProperty('--app-ui-header-scroll-padding', '32px')
    } else {
      document.documentElement.style.setProperty('--app-ui-header-scroll-padding', '64px')
      document.documentElement.style.setProperty('--app-ui-header-scroll-padding', '0px')
    }

    document.documentElement.style.setProperty('--app-ui-header-padding', 'calc(var(--app-ui-header-scroll-padding) + var(--app-ui-header-height))')
  }

  return {
    size,
    type,
    setHeaderType,
  }
}
