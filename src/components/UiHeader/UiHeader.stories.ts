import UiHeader from './UiHeader.vue'
import { useUiHeader, UiHeaderType } from './hooks/useUiHeader'

// More on default export: https://storybook.js.org/docs/vue/writing-stories/introduction#default-export
export default {
  title: 'Example/UiHeader',
  component: UiHeader,
}

const Template = (args) => ({
  components: { UiHeader },
  setup() {
    const { setHeaderType } = useUiHeader()
    setHeaderType(UiHeaderType.DEFAULT_DESKTOP)

    return { args }
  },
  template: `
    <div style="width: 100%" style="min-height: 2000px">
      <UiHeader v-bind="args"/>
      <img src="https://www.androidguys.com/wp-content/uploads/2016/03/wallpaper-full-hd-1080-x-1920-smartphone-space-alone-planet.jpg" style="width: 100%"/>
    </div>
  `,
})

const TemplateMobile = (args) => ({
  components: { UiHeader },
  setup() {
    const { setHeaderType } = useUiHeader()
    setHeaderType(UiHeaderType.DEFAULT_MOBILE)

    return { args }
  },
  template: `
    <div style="width: 100%" style="min-height: 2000px">
      <UiHeader v-bind="args"/>
      <img src="https://www.androidguys.com/wp-content/uploads/2016/03/wallpaper-full-hd-1080-x-1920-smartphone-space-alone-planet.jpg" style="width: 100%"/>
    </div>
  `,
})

export const Primary = Template.bind({})

Primary.args = {
  
  desktopMenu: [
    // {
    //   text: 'Swap',
    //   to: { name: 'swap' },
    // },
    {
      text: 'Presale',
      href: '#',
    },
    {
      text: 'Staking',
      href: '#',
    },
    {
      text: 'Referral',
      href: '#',
    },
    {
      text: 'Achievements',
      href: '#',
    }
  ],
  mobileMenu: [
    // {
    //   text: 'Swap',
    //   to: { name: 'swap' },
    // },
    {
      text: 'Presale',
      href: '#',
    },
    {
      text: 'Staking',
      href: '#',
    },
    {
      text: 'Referral',
      href: '#',
    },
    {
      text: 'Achievements',
      href: '#',
    }
  ],
}


export const PrimaryMobile = TemplateMobile.bind({})

PrimaryMobile.args = {
  
  desktopMenu: [
    // {
    //   text: 'Swap',
    //   to: { name: 'swap' },
    // },
    {
      text: 'Presale',
      href: '#',
    },
    {
      text: 'Staking',
      href: '#',
    },
    {
      text: 'Referral',
      href: '#',
    },
    {
      text: 'Achievements',
      href: '#',
    }
  ],
  mobileMenu: [
    // {
    //   text: 'Swap',
    //   to: { name: 'swap' },
    // },
    {
      text: 'Presale',
      href: '#',
    },
    {
      text: 'Staking',
      href: '#',
    },
    {
      text: 'Referral',
      href: '#',
    },
    {
      text: 'Achievements',
      href: '#',
    }
  ],
}
