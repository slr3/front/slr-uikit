import UiGalaxyLoader from './UiGalaxyLoader.vue';

// More on default export: https://storybook.js.org/docs/vue/writing-stories/introduction#default-export
export default {
  title: 'Example/UiGalaxyLoader',
  component: UiGalaxyLoader,
};

export const Primary = () => ({
  components: { UiGalaxyLoader },
  template: '<UiGalaxyLoader v-bind="args" />',
})
