import { UiClickableProps } from '../UiClickable'
import type { UI_BUTTON_VARIANTS } from './constants'

export type UiButtonValiantProp = typeof UI_BUTTON_VARIANTS[number]

export type UiButtonProps = UiClickableProps & {
  variant?: UiButtonValiantProp
  rounded?: boolean
}
