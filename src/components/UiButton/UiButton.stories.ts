import UiButton from './UiButton.vue';

// More on default export: https://storybook.js.org/docs/vue/writing-stories/introduction#default-export
export default {
  title: 'Example/UiButton',
  component: UiButton,
};

const Template = (args) => ({
  components: { UiButton },
  setup() {
    const sizes = [64, 48, 36, 32, 24]
    return { args, sizes };
  },
  template: `
    <div class="uikit-flex flex-wrap uikit-space-x-32">
      <div class="uikit-flex uikit-flex-col uikit-items-center uikit-space-y-12">
        <UiButton
          v-for="size in sizes"
          v-bind="args"
          :size="size"
          :key="size"
        >
          Ui Button!
        </UiButton>
      </div>
      <div class="uikit-flex uikit-flex-col uikit-items-center uikit-space-y-12">
        <UiButton
          v-for="size in sizes"
          v-bind="args"
          :size="size"
          :key="size"
        >
          1
        </UiButton>
      </div>
      <div class="uikit-flex uikit-flex-col uikit-items-center uikit-space-y-12">
        <UiButton
          rounded
          v-for="size in sizes"
          v-bind="args"
          :size="size"
          :key="size"
        >
          1
        </UiButton>
      </div>
    </div>
  `,
})

export const Primary = Template.bind({})

export const White = Template.bind({})
White.args = { variant: 'white' }

export const Violet = Template.bind({})
Violet.args = { variant: 'violet' }

export const Gray = Template.bind({})
Gray.args = { variant: 'gray' }

export const Red = Template.bind({})
Red.args = { variant: 'red' }
