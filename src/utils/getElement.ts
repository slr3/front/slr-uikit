import { isBrowser } from "./isBrowser";

export const getDocument = () => isBrowser && typeof document !== 'undefined' ? document : null
export const getBody = () => getDocument()?.body 
export const getWindow = () => isBrowser && typeof window !== 'undefined' ? window : null
