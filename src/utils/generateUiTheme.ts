import type { UiColor } from './colors'
import { calcContrastColor, calcLightenDarkenColor } from './colors'
import { isBrowser } from './isBrowser'

export declare type UiThemeBaseColors = Record<string, UiColor>

type UiThemeColors = UiThemeBaseColors

export declare interface UiTheme {
  colors: UiThemeColors,
}

const theme: UiTheme = {
  colors: {},
}

export const generateUiTheme = (
  colors: UiTheme['colors'],
) => {
  Object.entries(colors).forEach(([name, color]) => {
    const {colors: themeColors} = theme

    themeColors[name] = color
    themeColors[`${name}Contrast`] = calcContrastColor(color)
    themeColors[`${name}Darken`] = calcLightenDarkenColor(color, -0.1)
    themeColors[`${name}Lighten`] = calcLightenDarkenColor(color, 0.1)
  })
}

export const getUiTheme = () => theme

export const UIKIT_CSS_VARIABLES = {
  color: {
    text: {
      primary: '--ui-kit-text-color-primary',
      secondary: '--ui-kit-text-color-secondary',
    }
  }
}

export const applyUiTheme = () => {
  const css = [
    `${UIKIT_CSS_VARIABLES.color.text.primary}: rgba(254,254,254)`,
    `${UIKIT_CSS_VARIABLES.color.text.secondary}: rgba(254,254,254)`,
  ].join(';')

  if (isBrowser) {
    const style = document.createElement('style')

    style.innerHTML = css

    document.head.appendChild(style)
  }
}

