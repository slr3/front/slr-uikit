export const uiCalcRounded = (size: number) => {
  return Math.max(4, Math.ceil(size / 4))
}