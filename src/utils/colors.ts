export type UiColor = [number, number, number]

export const calcLightenDarkenColor = (color: UiColor, amt: number): UiColor => color.map(
  (val) => Math.max(0, Math.min(255, val + amt * 255))
) as UiColor

const colorContrastModifier = [0.299, 0.587, 0.114]
const colorDarkContrast: UiColor = [0, 0, 0]
const colorLightContrast: UiColor = [254, 254, 254]

export const calcContrastColor = (color:UiColor):UiColor => (
  colorContrastModifier
    .reduce((sum, p, index) => (sum + color[index] * p), 0) / 255 > 0.5 
      ? colorDarkContrast
      : colorLightContrast
)
