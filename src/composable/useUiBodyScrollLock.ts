import { useScrollLock } from '@vueuse/core'
import { readonly } from 'vue'
import { getBody } from '../utils/getElement'

/*
 * Если несколько раз вызвать useScrollLock, то может возникнуть ситуация когда скролл будет залочен до обновления страницы.
 * Поэтому используется такая обертка
 */

const isLocked = useScrollLock(getBody())

export const useUiBodyScrollLock = () => {
  const toggleLock = (value: boolean) => {
    if (typeof value === 'boolean' && value !== isLocked.value) {
      isLocked.value = value
    }
  }

  return {
    isLocked: readonly(isLocked),
    toggleLock,
  }
}
