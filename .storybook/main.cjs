const path = require('path');
const tailwindConfigPath = path.join(__dirname, '../tailwind.config.cjs'); // or your own config file
require('storybook-tailwind-foundations/initialize.js').default(tailwindConfigPath);

module.exports = {
   // ...
  stories: [
    // ... 
    
  ],
}

module.exports = {
  stories: [
    '../node_modules/storybook-tailwind-foundations/**/*.stories.js',
    '../src/**/*.stories.mdx',
    '../src/components/**/*.stories.ts'
  ],
  addons: [
    '@storybook/addon-links',
    '@storybook/addon-essentials',
    '@storybook/addon-interactions'
  ],
  framework: '@storybook/vue3',
  core: {
    builder: '@storybook/builder-vite'
  },
}
