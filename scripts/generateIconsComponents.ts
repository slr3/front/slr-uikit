import { readdirSync, lstatSync, writeFileSync, readFileSync } from 'fs'
import { join, dirname } from 'path'
import { fileURLToPath } from 'url'
import svgo from 'svgo'

const __filename = fileURLToPath(import.meta.url);
const __dirname = dirname(__filename);

const ICONS_DIR = join(__dirname, '../src/assets/svg')
const COMPONENTS_PATH = join(__dirname, '../src/components/UiIcon')
const GET_ASYNC_ICONS_FILE = join(COMPONENTS_PATH, 'getAsyncIcon.ts')
const GET_ICON_COMPONENT_FILE = join(COMPONENTS_PATH, 'getUiIcon.ts')

const toPascalCase = (str: string) => str.toLowerCase()
  .replace(new RegExp(/[-_]+/, 'g'), ' ')
  .replace(new RegExp(/[^\w\s]/, 'g'), '')
  .replace(
    new RegExp(/\s+(.)(\w*)/, 'g'),
    ($1, $2, $3) => `${$2.toUpperCase() + $3}`
  )
  .replace(new RegExp(/\w/), s => s.toUpperCase())

const getComponentName = (iconName: string) => `UiIcon${toPascalCase(iconName)}`
const getAsyncComponentName = (iconName: string) => `UiAsyncIcon${toPascalCase(iconName)}`

const getIcons = (dirPath: string, namePrefixs: string[] = []) => {
  const files = readdirSync(dirPath);
  const icons: {
    name: string,
    componentName: string,
    asyncComponentName: string,
    path: string
  }[] = []
  const iconsComponentsList: string[] = []

  files.forEach(file => {
    const filePath = join(dirPath, file)
    const namePrefix = namePrefixs.join('-')
    const name = (namePrefix ? `${namePrefix}-${file}` : file).replace(/.svg$/, '')
  
    if (lstatSync(filePath).isDirectory()) {
      const subDirIcons = getIcons(filePath, [...namePrefixs, name])

      icons.push(...subDirIcons.icons)
      iconsComponentsList.push(...subDirIcons.iconsComponentsList)
    } else if (file.endsWith('.svg')) {
      icons.push({
        name: name,
        componentName: getComponentName(name),
        asyncComponentName: getAsyncComponentName(name),
        path: filePath,
      })
      iconsComponentsList.push(name)
    }
  })
  return {
    icons,
    iconsComponentsList
  }
}

const { icons } = getIcons(ICONS_DIR)

const getAsyncIconsCode = `import { defineAsyncComponent, defineComponent, AsyncComponentLoader, h } from 'vue'
import { UiTextPlaceholder } from '../UiTextPlaceholder'

export const getAsyncIcon = (name: string, loader: AsyncComponentLoader) => {
  const icon = defineAsyncComponent({
    loader,
    loadingComponent: UiTextPlaceholder,
  })

  return defineComponent({
    name,
    render() {
      return h('div', [h(icon, { style: {width:'100%', height:'100%'} })])
    },
  })
}
`

writeFileSync(GET_ASYNC_ICONS_FILE, getAsyncIconsCode)

const getIconComponent = `import { defineComponent, h } from 'vue'
export const isBrowser: boolean = (() => typeof window === 'object')()

const initedIcons = new Set<string>()

let iconsMapElement: SVGSVGElement

if (isBrowser) {
  iconsMapElement = document.createElementNS('http://www.w3.org/2000/svg', 'svg');

  [['position', 'absolute'], ['width', '0px'], ['height', '0px']].forEach(([key, value]) => {
    iconsMapElement.style.setProperty(key, value)
  })

  document.body.appendChild(iconsMapElement)
}

export const getUiIcon = (name: string, symbol: string) => {
  if (!initedIcons.has(name)) {
    if (iconsMapElement) {
      iconsMapElement.innerHTML += symbol
    }

    initedIcons.add(name)
  }

  return defineComponent({
    name,
    render() {
      return h('svg', h('use', { 'xlink:href': '#' + name, 'aria-hidden': 'true' }))
    },
  })
}
`

writeFileSync(GET_ICON_COMPONENT_FILE, getIconComponent)

const handleSVGId = (componentName:string, svg: string): string => {
  const hasID = /="url\(#/.test(svg)
  const idMap: Record<string, string> = {}

  if (hasID) {
    svg = svg
      .replace(/\b([\w-]+?)="url\(#(.+?)\)"/g, (_, s, id) => {
        idMap[id] = `${componentName}-${id}`
        return `${s}=\"${`url(#${idMap[id]})`}\"`
      })
      .replace(/\bid="(.+?)"/g, (full, id) => {
        if (idMap[id])
          return `id=\"${idMap[id]}\"`
        return full
      })
  }

  return svg
}

const trimSVG = (str: string): string => {
	return (
		str
			// Replace multi lines attributes and keep one space between last " or ' of attribute and start letter of next attribute
			.replace(/(['"])\s*\n\s*([^>\\/\s])/g, '$1 $2')
			// Replace new line only after one of allowed characters that are not part of common attributes
			.replace(/(["';{}><])\s*\n\s*/g, '$1')
			// Keep one space in case it is inside attribute
			.replace(/\s*\n\s*/g, ' ')
			// Trim attribute values
			.replace(/\s+"/g, '"')
			.replace(/="\s+/g, '="')
			// Trim it
			.trim()
	);
}

const svgToSymbol = (componentName: string, svg: string): string => svg
  .replace(/^<svg/, `<symbol id="${componentName}"`)
  .replace(/<\/svg>$/, '</symbol>')

icons.forEach(({componentName, asyncComponentName, path}) => {
  // Create icons components
  const { data: svg } = svgo.optimize(
    readFileSync(path).toString(),
    {
      plugins: [
        'removeDimensions',
        'removeScriptElement',
        'reusePaths',
      ],
    }
  )
  let symbol = svgToSymbol(componentName, svg)
  symbol = handleSVGId(componentName, symbol)
  symbol = trimSVG(symbol)
  
  let iconCode = `import { getUiIcon } from './getUiIcon'\n`
  iconCode += `export const ${componentName} = getUiIcon('${componentName}', '${symbol}')\n`

  writeFileSync(join(COMPONENTS_PATH, `${componentName}.ts`), iconCode)

  // Create async icons components
  let asyncIconCode = `import { getAsyncIcon } from './getAsyncIcon'\n\n`
  asyncIconCode += `export const ${asyncComponentName} = getAsyncIcon('${asyncComponentName}', () => import(\'./${componentName}').then(({${componentName}}) => ${componentName}))`
  writeFileSync(join(COMPONENTS_PATH, `${asyncComponentName}.ts`), asyncIconCode)
})

const indexCode = `${icons.map(({componentName, asyncComponentName}) => `export { ${componentName} } from './${componentName}'\nexport { ${asyncComponentName} } from './${asyncComponentName}'`).join('\n')}`

writeFileSync(join(COMPONENTS_PATH, `index.ts`), indexCode)

console.log(icons.map(({componentName}) => componentName).join('\n'))