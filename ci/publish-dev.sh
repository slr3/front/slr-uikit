#!/bin/bash  

yarn build
echo "Update version\n\n"
npx json -I -f ./package.json -e "this.version+=\"-${CI_COMMIT_SHORT_SHA}\""
echo -e "\033[1m  NEW VERSION: $(npx json -f ./package.json version) \033[0m\n\n"
yarn npm publish