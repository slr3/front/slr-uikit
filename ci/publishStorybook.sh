#!/bin/bash  

if [[ -z "${CHROMATIC_TOKEN}" ]]; then
  echo "CHROMATIC_TOKEN is not defined"
  exit 1
else
  npx chromatic --project-token $CHROMATIC_TOKEN --exit-zero-on-changes --ci
fi
